const jwt = require("jsonwebtoken");
const secret = "E-CommerceAPI";

module.exports.createAccessToken = (userCredentials) => {

	const userData = {
		id: userCredentials._id,
		email: userCredentials.email,
		isAdmin: userCredentials.isAdmin
	}

	return jwt.sign(userData, secret, {});

}

module.exports.tokenVerification = (request, response, next) => {

	let token = request.headers.authorization;

	if (typeof token !== "undefined") {

		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (error, data) => {

			if (error) {
				return res.send({authentication: "failed"});
			} else {
				next();
			}

		});

	} else {
		return res.send({authentication: "failed"});
	}

}

module.exports.tokenDecryption = (token) => {

	if (typeof token !== "undefined") {

		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (error, data) => {

			if (error) {
				return "Token Decryption failed.";
			} else {
				return jwt.decode(token, {complete: true}).payload
			}

		});

	} else {
		return "Token does not exist!";
	}


}