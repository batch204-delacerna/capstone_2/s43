const express = require("express");
const router = express.Router();
const usersController = require("../controllers/usersController");
const authentication = require("../auth");


// User Registration route
router.post("/register", (request, response) => {
	usersController.userRegistration(request.body).then(controllerResult => response.send(controllerResult));
});


// User Authentication route
router.post("/login", (request, response) => {
	usersController.userLogin(request.body).then(controllerResult => response.send(controllerResult));
});





module.exports = router;